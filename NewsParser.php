<?php
include "simple_html_dom.php";


class NewsParser {


    /**
     * Return the title of the Penutilmate news of the given website 
     * 
     */
    public function getTitleOfThePenultimateNews($url = "http://www.infobae.com")
    {
        $list_news = $this->parseNewsOf($url);
    
        return trim(strip_tags($list_news[count($list_news) - 2]['title']));
    }

    /**
     * Return array of news from the given $url
     * 
     */
    protected function parseNewsOf($url) 
    {
        $dom = file_get_html($url);
    
        $list_news = [];
    
        foreach ($dom->find('div[class="column-item"]>h4>a') as $news) {
            $list_news[] = [
                'title' => $news->innertext,
                'href' => $news->href
            ];    
        }
    
        return $list_news;
    }

}